package wedoogift.challenge.level1.solution.model;

import lombok.Data;

/**
 * Created by Yang SHI the 07/02/2021
 * This class represents the model of company entity
 */


@Data
public class Company extends AbstractBalanceEntity {
    private String name;

    public Company(int id, double balance, String name) {
        this.id = id;
        this.balance = balance;
        this.name = name;
    }
}
