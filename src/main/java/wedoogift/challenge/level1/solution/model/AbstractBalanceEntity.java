package wedoogift.challenge.level1.solution.model;

import lombok.*;

/**
 * Created by Yang SHI the 07/02/2021
 * This class represents the model of entity with a balance. This is an abstract class.
 */

@Getter
@Setter
@EqualsAndHashCode
@ToString
public abstract class AbstractBalanceEntity {
    protected int id;
    protected double balance;
}
