package wedoogift.challenge.level1.solution.model;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Created by Yang SHI the 07/02/2021
 * This class represents the model of user entity
 */

@Data
public class User extends AbstractBalanceEntity {
    private List<Distribution> listDistribution;

    public User (int id, double balance, List<Distribution> listDistribution) {
        this.id = id;
        this.balance = balance;
        this.listDistribution = listDistribution;
    }

    public double getUserBalance() {
        Date currentDate = new Date();
        return balance + listDistribution.stream()
                .filter(distribution -> (currentDate.after(distribution.getStartDate()) && currentDate.before(distribution.getEndDate()))
                        || currentDate.equals(distribution.getStartDate()) || currentDate.equals(distribution.getEndDate()))
                .mapToDouble(Distribution::getAmount).sum();
    }
    
}
