package wedoogift.challenge.level1.solution.service;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.json.simple.JSONObject;
import wedoogift.challenge.level1.solution.model.Company;
import wedoogift.challenge.level1.solution.model.Distribution;
import wedoogift.challenge.level1.solution.model.User;
import wedoogift.challenge.level1.solution.utils.Constant;

@Service
public class JsonFileService {

    /**
     * Read the inputJson
     *
     * @return the extracted Json from the file
     * @throws IOException
     * @throws ParseException
     */
    public JSONObject readInputJsonFile() throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader("input.json");
        Object inputFileJsonList = jsonParser.parse(reader);
        JSONObject jasonObject = (JSONObject) inputFileJsonList;
        return jasonObject;
    }

    /**
     * Initialise the company List from the extracted Json
     *
     * @param companyJson
     * @return the initialized company list
     */
    public List<Company> initializeCompany(JSONArray companyJson) {
        List<Company> listCompanyToBuild = new ArrayList<>();
        companyJson.forEach(company -> {
            Company companyToBuild = new Company(Integer.valueOf(((JSONObject) company).get(Constant.FIELD_ID).toString()),
                    Double.valueOf(((JSONObject) company).get(Constant.FIELD_BALANCE).toString()),
                    ((JSONObject) company).get(Constant.FIELD_NAME).toString());
            listCompanyToBuild.add(companyToBuild);
        });
        return listCompanyToBuild;
    }

    /**
     * Initialise the user List from the extracted Json
     *
     * @param userJson
     * @return the initialized user list
     */
    public List<User> initializeUser(JSONArray userJson) {
        List<User> listUserToBuild = new ArrayList<>();
        userJson.forEach(user -> {
            User userToBuild = new User(Integer.valueOf(((JSONObject) user).get(Constant.FIELD_ID).toString()), Double.valueOf(((JSONObject) user).get(Constant.FIELD_BALANCE).toString()), new ArrayList<>());
            listUserToBuild.add(userToBuild);
        });
        return listUserToBuild;
    }

    /**
     * Method which write an output.json from the companies and users
     *
     * @param listCompany
     * @param listUser
     * @throws IOException
     */
    public void writeInputJsonFile(List<Company> listCompany, List<User> listUser) throws IOException {
        JSONArray listCompanyJson = generateCompanyJson(listCompany);
        JSONArray listUserJson = generateUserJson(listUser);
        JSONArray listDistributionJson = generateDistributionJson(listUser);

        JSONObject jsonToWrite = new JSONObject();
        jsonToWrite.put(Constant.COMPANIES, listCompanyJson);
        jsonToWrite.put(Constant.USERS, listUserJson);
        jsonToWrite.put(Constant.DISTRIBUTIONS, listDistributionJson);
        FileWriter file = new FileWriter("output.json");
        file.write(jsonToWrite.toJSONString());
        file.flush();
    }

    /**
     * Generate the company Json to write
     *
     * @param listCompany
     * @return the converted company Json to write
     */
    private JSONArray generateCompanyJson(List<Company> listCompany) {
        JSONArray listCompanyJson = new JSONArray();
        listCompany.stream().forEach(company -> {
            JSONObject companyJson = new JSONObject();
            companyJson.put(Constant.FIELD_ID, company.getId());
            companyJson.put(Constant.FIELD_NAME, company.getName());
            companyJson.put(Constant.FIELD_BALANCE, company.getBalance());
            listCompanyJson.add(companyJson);
        });
        return listCompanyJson;
    }

    /**
     * Generate the user Json to write
     *
     * @param listUser
     * @return the converted user Json to write
     */
    private JSONArray generateUserJson(List<User> listUser) {
        JSONArray listUserJson = new JSONArray();
        listUser.stream().forEach(user -> {
            JSONObject userJson = new JSONObject();
            userJson.put(Constant.FIELD_ID, user.getId());
            userJson.put(Constant.FIELD_BALANCE, user.getUserBalance());
            listUserJson.add(userJson);
        });

        return listUserJson;
    }

    /**
     * Generate the distribution Json to write
     * @param listUser
     * @return the converted distribution Json to write
     */
    private JSONArray generateDistributionJson(List<User> listUser) {
        JSONArray listDistributionJson = new JSONArray();
        List<Distribution> listDistributionOfAllUser = new ArrayList<>();
        listUser.stream().forEach(user -> {
            if(user.getListDistribution() == null || user.getListDistribution().isEmpty()) {
                return;
            }
            listDistributionOfAllUser.addAll(user.getListDistribution());
        });

        listDistributionOfAllUser.stream().sorted(Comparator.comparingInt(Distribution::getId)).forEach(distribution -> {
            JSONObject distributionJson = new JSONObject();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            distributionJson.put(Constant.FIELD_ID, distribution.getId());
            distributionJson.put(Constant.FIELD_AMOUNT, distribution.getAmount());
            distributionJson.put(Constant.FIELD_START_DATE, formatter.format(distribution.getStartDate()));
            distributionJson.put(Constant.FIELD_END_DATE, formatter.format(distribution.getEndDate()));
            distributionJson.put(Constant.FIELD_COMPANY_ID, distribution.getIdCompany());
            distributionJson.put(Constant.FIELD_USER_ID, distribution.getIdUser());
            listDistributionJson.add(distributionJson);
        });
        return listDistributionJson;
    }
}
