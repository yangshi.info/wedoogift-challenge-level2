package wedoogift.challenge.level1.solution.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import wedoogift.challenge.level1.solution.model.Company;

import static wedoogift.challenge.level1.solution.utils.Constant.MINIMUM_BALANCE_VALUE;

@Service
public class CompanyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyService.class);

    /**
     * Method which check the company balance and perform a change on its balance
     * @param company
     * @param amount to change on the company balance
     * @return Company after change, if the change is impossible, the returned company remains the same
     */
    public Company changeCompanyBalance(Company company, double amount) {
        if (company == null) {
            LOGGER.error("The company shouldn't be null. The balance isn't changed");
            return null;
        }

        double companyCurrentBalance = company.getBalance();

        if (companyCurrentBalance + amount < MINIMUM_BALANCE_VALUE) {
            LOGGER.error("The amount to change#{} makes the company#{} balance negative. Current company balance : {} . The balance isn't changed", amount, company.getName(), companyCurrentBalance);
        } else {
            company.setBalance(companyCurrentBalance + amount);
            LOGGER.info("The amount to change#{} for the company#{} is proceed. This is the company new balance : ", amount, company.getName(), company.getBalance());
        }

        return company;
    }

}
