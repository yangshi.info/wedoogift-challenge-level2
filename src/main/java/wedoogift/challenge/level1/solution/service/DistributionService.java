package wedoogift.challenge.level1.solution.service;

import org.springframework.stereotype.Service;
import wedoogift.challenge.level1.solution.model.Company;
import wedoogift.challenge.level1.solution.model.Distribution;
import wedoogift.challenge.level1.solution.model.User;
import wedoogift.challenge.level1.solution.utils.Constant;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Service
public class DistributionService {

    /**
     * Simple methode which build a new distribution
     * @param id
     * @param company
     * @param user
     * @param amount
     * @param startDate
     * @return built Distribution
     */
    public Distribution buildNewDistribution(int id, Company company, User user, double amount, Date startDate) {
        LocalDateTime localDateTime = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.plusDays(Constant.END_DATE_DAY_NUMBER);
        Date calculatedEndDate = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        return new Distribution(id, amount, startDate, calculatedEndDate, company.getId(), user.getId());
    }

}
