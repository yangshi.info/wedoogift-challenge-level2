package wedoogift.challenge.level1.solution.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wedoogift.challenge.level1.solution.model.Company;
import wedoogift.challenge.level1.solution.model.Distribution;
import wedoogift.challenge.level1.solution.model.User;

import java.util.ArrayList;
import java.util.Date;

@Service
public class BalanceTransactionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BalanceTransactionService.class);

    @Autowired
    CompanyService companyService;

    @Autowired
    DistributionService distributionService;

    /**
     * This method allows a company to give a distribution to a user
     *
     *
     * @param company which gives the distribution
     * @param user who get the distribution
     * @param amount of money the user get from the distribution
     * @param idDistribution technical id of the distribution
     * @param startDate the distribution start date
     */
    public void giveGiftCard(Company company, User user, double amount, int idDistribution, Date startDate) {
        if(company == null || user == null) {
            LOGGER.info("Company or user is null. Exiting the transaction");
            return;
        }
        double oldCompanyBalance = company.getBalance();
        company = companyService.changeCompanyBalance(company, -amount);
        if(company.getBalance() == oldCompanyBalance) {
            LOGGER.info("No change for the company#{} balance", company.getName());
            return;
        } else {
            Distribution distribution = distributionService.buildNewDistribution(idDistribution, company, user, amount, startDate);
            if(user.getListDistribution() == null) {
                user.setListDistribution(new ArrayList<>());
            }
            user.getListDistribution().add(distribution);
        }
    }
}
