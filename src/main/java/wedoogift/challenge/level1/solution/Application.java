package wedoogift.challenge.level1.solution;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import wedoogift.challenge.level1.solution.model.Company;
import wedoogift.challenge.level1.solution.model.User;
import wedoogift.challenge.level1.solution.service.BalanceTransactionService;
import wedoogift.challenge.level1.solution.service.JsonFileService;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@SpringBootApplication
public class Application implements CommandLineRunner {
	@Autowired
	JsonFileService jsonFileService;

	@Autowired
	BalanceTransactionService balanceTransactionService;

	private static List<Company> listCompany = new ArrayList<>();

	private static List<User> listUser = new ArrayList<>();

	private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(Application.class);
		app.run();
	}

	@Override
	public void run(String... args) {
		try {
			JSONObject inputJson = jsonFileService.readInputJsonFile();
			LOGGER.info("Extracted Json : {}", inputJson);
			JSONArray companyJson = (JSONArray) inputJson.get("companies");
			listCompany = jsonFileService.initializeCompany(companyJson);
			LOGGER.info("Initialized Company : {}", listCompany);
			JSONArray userJson = (JSONArray) inputJson.get("users");
			listUser = jsonFileService.initializeUser(userJson);
			LOGGER.info("Initialized User : {}", listUser);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.FRENCH);
			balanceTransactionService.giveGiftCard(listCompany.get(0), listUser.get(0), 50, 1, formatter.parse("2020-09-16"));
			balanceTransactionService.giveGiftCard(listCompany.get(0), listUser.get(1), 100, 2, formatter.parse("2020-08-01"));
			balanceTransactionService.giveGiftCard(listCompany.get(1), listUser.get(2), 1000, 3, formatter.parse("2020-05-01"));
			LOGGER.info("Writing in the output.json...");
			jsonFileService.writeInputJsonFile(listCompany, listUser);
			LOGGER.info("File written. The program ends here.");
		} catch (IOException e) {
			LOGGER.error("Error during Json extracting : {}", e.getMessage());
		} catch (ParseException e) {
			LOGGER.error("Error during Json parsing : {}", e.getMessage());
		} catch (java.text.ParseException e) {
			LOGGER.error("Error during Date parsing : {}", e.getMessage());
		}
	}
}
