package wedoogift.challenge.level1.solution.utils;

public class Constant {
    public static double MINIMUM_BALANCE_VALUE = 0;
    public static long END_DATE_DAY_NUMBER = 364;
    public static String FIELD_ID = "id";
    public static String FIELD_NAME = "name";
    public static String FIELD_BALANCE = "balance";
    public static String FIELD_AMOUNT = "amount";
    public static String FIELD_START_DATE = "start_date";
    public static String FIELD_END_DATE = "end_date";
    public static String FIELD_COMPANY_ID = "company_id";
    public static String FIELD_USER_ID = "user_id";
    public static String COMPANIES = "companies";
    public static String USERS = "users";
    public static String DISTRIBUTIONS = "distributions";
}
